# -*- coding: utf-8 -*-

"""This module handles exporting schematic/layout from Virtuoso.
"""

from abc import ABC
import os
from typing import TYPE_CHECKING, Optional, Dict, Any

from bag.io import write_file, open_temp

from bag.concurrent.core import ProcInfo
from bag.verification.base import SubProcessChecker
from bag.interface.database import DbAccess

from opensource.xschem import XSchemLibraryManager, NetlistMode, xschem_netlist_command


class OpenSourceChecker(SubProcessChecker, ABC):
    """the base Checker class for Open Source Tools.

    This class implement layout/schematic export procedures.

    Parameters
    ----------
    tmp_dir : str
        temporary file directory.
    max_workers : int
        maximum number of parallel processes.
    cancel_timeout : float
        timeout for cancelling a subprocess.
    source_added_file : str
        file to include for schematic export.
    interface : DbAccess
        Configuration database
    """

    def __init__(self,
                 tmp_dir: str,
                 max_workers: int,
                 cancel_timeout: float,
                 interface: DbAccess):
        super().__init__(tmp_dir, max_workers, cancel_timeout)
        self._interface = interface

    def setup_export_layout(self,
                            lib_name: str,
                            cell_name: str,
                            out_file: str,
                            view_name: str = 'layout',
                            params: Optional[Dict[str, Any]] = None) -> ProcInfo:
        gds_file = os.path.join(self._interface.default_lib_path, lib_name + ".gds")
        out_file = os.path.abspath(out_file)

        run_dir = os.path.dirname(out_file)
        out_name = os.path.basename(out_file)
        log_file = os.path.join(run_dir, 'layout_export.log')

        os.makedirs(run_dir, exist_ok=True)

        cmd = ['cp', gds_file, out_file]
        return cmd, log_file, None, run_dir

    def setup_export_schematic(self,
                               lib_name: str,
                               cell_name: str,
                               out_file: str,
                               view_name: str = 'schematic',
                               params: Optional[Dict[str, Any]] = None) -> ProcInfo:
        out_file = os.path.abspath(out_file)
        out_dir = os.path.dirname(out_file)
        log_file = os.path.join(out_dir, 'schematic_export.log')
        
        cwd = self._interface.xschem_project_dir
        xschemrc = os.path.join(cwd, "xschemrc")
        xschem_lib_mgr = XSchemLibraryManager.cached_instance(xschemrc_file=xschemrc)

        sch = xschem_lib_mgr.schematic(lib_name, cell_name)

        cmd = xschem_netlist_command(xschemrc_file=xschemrc,
                                     schematic_path=sch.schematic_file,
                                     out_path=out_file,
                                     mode=NetlistMode.LVS)

        return cmd, log_file, None, cwd
