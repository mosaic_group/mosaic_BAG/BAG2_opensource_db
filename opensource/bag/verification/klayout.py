import typing
import os

from .opensource import OpenSourceChecker
from bag.concurrent.core import FlowInfo
from bag.io import read_file, open_temp, readlines_iter


def pass_on_exit_code0(retcode, log_file):
    return [retcode == 0, log_file]


def _format_param(p: str) -> str:
    """
    Formats a parmameter for use in RSF file
    Note: Ruby and Python are close enough so we can use repr(...) - hopefully
    """
    return repr(p)


class KLayoutChecker(OpenSourceChecker):

    def __init__(self, tmp_dir, **kwargs):
        self.lvs_run_dir = kwargs.get("lvs_run_dir", None)
        self.lvs_runset = kwargs.get("lvs_runset", None)
        self.default_lvs_params = kwargs.get('lvs_params', {})

        self.drc_run_dir = kwargs.get("drc_run_dir", None)
        self.drc_runset = kwargs.get("drc_runset", None)
        self.default_drc_params = kwargs.get('drc_params', {})

        interface = kwargs.get('interface', None)
        max_workers = kwargs.get('max_workers', None)
        cancel_timeout = kwargs.get('cancel_timeout_ms', None)

        if cancel_timeout is not None:
            cancel_timeout /= 1e3

        super().__init__(tmp_dir, max_workers, cancel_timeout, interface)

    def setup_drc_flow(self, lib_name, cell_name,
                       lay_view='layout', params=None, **kwargs):

        if self.drc_run_dir is None:
            raise Exception("DRC not supported: 'drc_run_dir' not configured")
        if self.drc_runset is None:
            raise Exception("DRC not supported: 'drc_runset' not configured")

        run_dir = os.path.join(self.drc_run_dir, lib_name, cell_name)
        os.makedirs(run_dir, exist_ok=True)
        lay_file = self._get_lay_file(run_dir)

        # add schematic/layout export to flow
        flow_list = []

        # Check if gds layout is provided
        gds_layout_path = kwargs.pop('gds_layout_path', None)

        # If not provided the gds layout, need to export layout
        if not gds_layout_path:
            cmd, log, env, cwd = self.setup_export_layout(lib_name, cell_name, lay_file, lay_view, None)
            flow_list.append((cmd, log, env, cwd, pass_on_exit_code0))
        # If provided gds layout, do not export layout, just copy gds
        else:
            if not os.path.exists(gds_layout_path):
                raise ValueError(f'gds_layout_path does not exist: {gds_layout_path}')
            with open_temp(prefix='copy', dir=run_dir, delete=True) as f:
                copy_log_file = f.name
            copy_cmd = ['cp', gds_layout_path, os.path.abspath(lay_file)]
            flow_list.append((copy_cmd, copy_log_file, None, None, pass_on_exit_code0))

        drc_params_actual = self.default_drc_params.copy()
        if params is not None:
            drc_params_actual.update(params)

        # TODO: the original version writes a new file all the time. Why?
        # with open_temp(prefix='drcLog', dir=run_dir, delete=False) as logf:
        #  log_file = logf.name
        log_file = os.path.join(run_dir, "klayout_drc.log")
        report_file = os.path.join(run_dir, "klayout_drc.lyrdb")

        # generate new runset
        runset_content = [
            "$input=" + _format_param(lay_file),
            "$top_cell=" + _format_param(cell_name),
            "$report=" + _format_param(report_file)
        ]
        for k, v in drc_params_actual:
            runset_content.append(k + "=" + _format_param(v))

        runset_content.append("")
        runset_content.append("# %include " + self.drc_runset)

        # save runset
        # TODO: the original version writes a new file all the time. Why?
        # with open_temp(dir=run_dir, delete=False) as runset_file:
        #  runset_fname = runset_file.name
        #  runset_file.write("\n".join(runset_content))
        runset_fname = os.path.join(run_dir, "klayout_drc.drc")
        with open(runset_fname, "w") as runset_file:
            runset_file.write("\n".join(runset_content))

        print("DRC runset written to:\n" + runset_fname)
        print("DRC log file:\n" + log_file)
        cmd = ['klayout', '-b', '-r', runset_fname]

        flow_list.append((cmd, log_file, None, run_dir, pass_on_exit_code0))

        return flow_list

    def get_rcx_netlists(self, lib_name: str, cell_name: str) -> None:
        raise Exception("RCX not supported in KLayout currently")

    def setup_rcx_flow(self, lib_name: str, cell_name: str, sch_view: str = 'schematic', lay_view: str = 'layout',
                       params=None, **kwargs) -> [FlowInfo]:
        raise Exception("RCX not supported in KLayout currently")

    @classmethod
    def _get_lay_file(cls, run_dir: str) -> str:
        return os.path.join(run_dir, 'layout.gds')

    @classmethod
    def _get_sch_file(cls, run_dir: str) -> str:
        return os.path.join(run_dir, 'schematic.net')

    def setup_lvs_flow(self, lib_name: str, cell_name: str, sch_view: str = 'schematic', lay_view: str = 'layout',
                       params=None, **kwargs) -> [FlowInfo]:

        if self.lvs_run_dir is None:
            raise Exception("LVS not supported: 'lvs_run_dir' not configured")
        if self.lvs_runset is None:
            raise Exception("LVS not supported: 'lvs_runset' not configured")

        run_dir = os.path.join(self.lvs_run_dir, lib_name, cell_name)
        os.makedirs(run_dir, exist_ok=True)
        lay_file = self._get_lay_file(run_dir)
        sch_file = self._get_sch_file(run_dir)

        # add schematic/layout export to flow
        flow_list = []

        # Check if gds layout is provided
        gds_layout_path = kwargs.pop('gds_layout_path', None)

        # If not provided the gds layout, need to export layout
        if not gds_layout_path:
            cmd, log, env, cwd = self.setup_export_layout(lib_name, cell_name, lay_file, lay_view, None)
            flow_list.append((cmd, log, env, cwd, pass_on_exit_code0))
        # If provided gds layout, do not export layout, just copy gds
        else:
            if not os.path.exists(gds_layout_path):
                raise ValueError(f'gds_layout_path does not exist: {gds_layout_path}')
            with open_temp(prefix='copy', dir=run_dir, delete=True) as f:
                copy_log_file = f.name
            copy_cmd = ['cp', gds_layout_path, os.path.abspath(lay_file)]
            flow_list.append((copy_cmd, copy_log_file, None, None, pass_on_exit_code0))

        cmd, log, env, cwd = self.setup_export_schematic(lib_name, cell_name, sch_file, sch_view, None)
        flow_list.append((cmd, log, env, cwd, pass_on_exit_code0))

        lvs_params_actual = self.default_lvs_params.copy()
        if params is not None:
            lvs_params_actual.update(params)

        # TODO: the original version writes a new file all the time. Why?
        # with open_temp(prefix='lvsLog', dir=run_dir, delete=False) as logf:
        #  log_file = logf.name
        log_file = os.path.join(run_dir, "klayout_lvs.log")
        report_file = os.path.join(run_dir, "klayout_lvs.lvsdb")

        # generate new runset
        runset_content = [
            "$input=" + _format_param(lay_file),
            "$top_cell=" + _format_param(cell_name),
            "$schematic=" + _format_param(sch_file),
            "$report=" + _format_param(report_file)
        ]
        for k, v in lvs_params_actual:
            runset_content.append(k + "=" + _format_param(v))

        runset_content.append("")
        # xschem writes netlists without top subckt -> these are named .TOP by KLayout's netlist reader
        runset_content.append("same_circuits($top_cell, \".TOP\")")
        runset_content.append("")
        runset_content.append("# %include " + self.lvs_runset)

        # save runset
        # TODO: the original version writes a new file all the time. Why?
        # with open_temp(dir=run_dir, delete=False) as runset_file:
        #  runset_fname = runset_file.name
        #  runset_file.write("\n".join(runset_content))
        runset_fname = os.path.join(run_dir, "klayout_lvs.lvs")
        with open(runset_fname, "w") as runset_file:
            runset_file.write("\n".join(runset_content))

        print("LVS runset written to:\n" + runset_fname)
        print("LVS log file:\n" + log_file)
        cmd = ['klayout', '-b', '-r', runset_fname]

        flow_list.append((cmd, log_file, None, run_dir, pass_on_exit_code0))

        return flow_list
