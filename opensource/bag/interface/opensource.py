# -*- coding: utf-8 -*-

"""This module implements manipulations of the xschem file format.
"""

from typing import *

from functools import cached_property
import os
import re
import shutil
import subprocess
import yaml

import bag.layout.core
from bag.io.common import get_encoding, fix_string
from bag.io.file import open_temp
from bag.interface.database import DbAccess

from opensource.xschem import XSchemLibraryManager, XSchemSchematic, NetlistMode
from opensource.layout_generator import make_layout_generator


class OpenSourceInterface(DbAccess):
    """Interface between bag and xschem.

    This class performs all bag's database and simulation operations on
    external xschem files.

    Parameters
    ----------
    dealer : :class:`bag.interface.ZMQDealer`
    TODO
        the socket used to communicate with :class:`~bag.interface.SkillOceanServer`.
    tmp_dir : string
        temporary file directory for DbAccess.
    db_config : dict[str, any]
        the database configuration dictionary.
    """

    def __init__(self, dealer, tmp_dir, db_config):
        """Initialize a new XyceInterface object."""
        super().__init__(tmp_dir, db_config)
        self.handler = dealer
        self._rcx_jobs = {}

        # read opensource configuration
        # This first of all is a hash that configures the open source DbAccess module
        # The configuration file is a YAML file called "opensource.yaml" which is
        # looked up in the current directory or $BAG_TECH_CONFIG_DIR if specified

        opensource_cfg = "opensource.yaml"
        bag_tech_config_dir_var = "BAG_TECH_CONFIG_DIR"

        cfg_paths = [opensource_cfg]
        if bag_tech_config_dir_var in os.environ:
            cfg_paths.append(os.path.join(os.environ[bag_tech_config_dir_var], opensource_cfg))

        self.config = {}

        for p in cfg_paths:
            if os.path.exists(p):
                with open(p, "r") as stream:
                    self.config = yaml.safe_load(stream)
                break

        xschemrc_file = os.path.join(self.default_lib_path, "xschem", "xschemrc")
        self.xschem_library_manager = XSchemLibraryManager.cached_instance(xschemrc_file=xschemrc_file)
        self.xschem_library_manager.update_from_rc_template(self.xschemrc_template_path)

    def validate_bag_server_availability(self, timeout: int = 5000):
        """Verify that the database server is available"""
        # there is no server, hence we can simply skip this test
        pass

    def close(self):
        """Terminate the database server gracefully."""
        # self.handler.send_obj(dict(type='exit'))
        # self.handler.close()
        pass

    @cached_property
    def xschemrc_template_path(self) -> str:
        """
        Path to xschemrc template (that does not contain additional generated libraries yet)
        """
        return os.path.join(os.environ["BAG_WORK_DIR"], "xschem", "xschemrc.in")

    @cached_property
    def xschem_project_dir(self) -> str:
        project_dir_path = os.path.join(self.default_lib_path, "xschem")
        return project_dir_path

    @cached_property
    def xschemrc_user_path(self) -> str:
        """
        Gets the path where the users's xschemrc is stored.
        """
        project_rc = os.path.join(self.xschem_project_dir, "xschemrc")
        return project_rc

    def parse_schematic_template(self, lib_name: str, cell_name: str) -> str:
        """Parse the given schematic template.

        Parameters
        ----------
        lib_name : str
            name of the library.
        cell_name : str
            name of the cell.

        Returns
        -------
        template : str
            the content of the netlist structure file.
        """
        # cmd = 'parse_cad_sch( "%s" "%s" {netlist_info} )' % (lib_name, cell_name)
        # template = self._eval_skill(cmd, out_file='netlist_info')

        sch = self.xschem_library_manager.schematic(lib_name=lib_name, cell_name=cell_name)
        template = sch.netlist_yaml
        return template

    @staticmethod
    def _get_xschem_library_name(lib_name: str) -> str:
        """
        # Structure for generator packages to be compatible with both virtuoso_template and opensource_db_template:
        #       <cellname>_templates is the OA/Virtuoso version (of the symbol and template schematic)
        #       <cellname>_templates_xschem is the opensource DB version (of the symbol and template schematic)
        """
        xschem_lib_name = f'{lib_name}_xschem'
        return xschem_lib_name

    def get_cells_in_library(self, lib_name: str) -> List[str]:
        """Get a list of cells in the given library.

        Returns an empty list if the given library does not exist.

        Parameters
        ----------
        lib_name : str
            the library name.

        Returns
        -------
        cell_list : list[str]
            a list of cells in the library
        """

        xschem_lib_name = self._get_xschem_library_name(lib_name)
        cell_list = self.xschem_library_manager.cells_of_library(lib_name=xschem_lib_name)
        return cell_list

    def create_library(self, lib_name: str, lib_path: str = ""):
        """Create a new library if one does not exist yet.

        Parameters
        ----------
        lib_name : string
            the library name.
        lib_path : string
            directory to create the library in.  If Empty, use default location.
        """

        # add to the xschem directory path
        lib_path = lib_path or self.default_lib_path
        self.xschem_library_manager.add_library(lib_name=lib_name, lib_path=lib_path, replace_if_exists=True)

    def create_implementation(self,
                              lib_name: str,
                              template_list: List,
                              change_list: List,
                              lib_path: str = ""):
        """Create implementation of a design in the CAD database.

        Parameters
        ----------
        lib_name : str
            implementation library name.
        template_list : list
            a list of schematic templates to copy to the new library.
        change_list :
            a list of changes to be performed on each copied templates.
        lib_path : str
            directory to create the library in.  If Empty, use default location.
        """
        lib_path = lib_path or self.default_lib_path
        self.xschem_library_manager.create_implementation_library(lib_name=lib_name,
                                                                  template_list=template_list,
                                                                  change_list=change_list,
                                                                  lib_path=lib_path)

    def configure_testbench(self, tb_lib: str, tb_cell: str) -> Tuple[
                                                                    str,
                                                                    List[str],
                                                                    Dict[str, str]
                                                                ]:
        """Update testbench state for the given testbench.

        This method fill in process-specific information for the given testbench.

        Parameters
        ----------
        tb_lib : str
            testbench library name.
        tb_cell : str
            testbench cell name.

        Returns
        -------
        cur_env : str
            the current simulation environment.
        envs : list[str]
            a list of available simulation environments.
        parameters : dict[str, str]
            a list of testbench parameter values, represented as string.
        """
        #
        # tb_config = self.db_config['testbench']
        #
        # cmd = ('instantiate_testbench("{tb_cell}" "{targ_lib}" ' +
        #        '"{config_libs}" "{config_views}" "{config_stops}" ' +
        #        '"{default_corner}" "{corner_file}" {def_files} ' +
        #        '"{tech_lib}" {result_file})')
        # cmd = cmd.format(tb_cell=tb_cell,
        #                  targ_lib=tb_lib,
        #                  config_libs=tb_config['config_libs'],
        #                  config_views=tb_config['config_views'],
        #                  config_stops=tb_config['config_stops'],
        #                  default_corner=tb_config['default_env'],
        #                  corner_file=tb_config['env_file'],
        #                  def_files=to_skill_list_str(tb_config['def_files']),
        #                  tech_lib=self.db_config['schematic']['tech_lib'],
        #                  result_file='{result_file}')
        # output = yaml.load(self._eval_skill(cmd, out_file='result_file'), Loader=yaml.Loader)
        # return tb_config['default_env'], output['corners'], output['parameters'], output['outputs']

        sch = self.xschem_library_manager.schematic(lib_name=tb_lib, cell_name=tb_cell)
        sch_path = sch.schematic_file

        # {'data': 'corners:\n'
        #          '  - FF\n'
        #          '  - SS\n'
        #          '  - TT\n'
        #          '  - FS\n'
        #          '  - SF\n'
        #          '  - MC\n'
        #          'enabled_corners:\n'
        #          '  - TT\n'
        #          'parameters:\n'
        #          '  ibias: "100u"\n'
        #          '  vdd: "1"\n'
        #          '  vinac: "1"\n'
        #          '  vindc: "0.5"\n'
        #          '  fstart: "1M"\n'
        #          '  fstop: "10e9"\n'
        #          '  fndec: "20"\n'
        #          '  tsim: "1n"\n'
        #          '  tstep: "1p"\n'
        #          '  cload: "10f"\n'
        #          'outputs:\n'
        #          '  "vin_tran": !!str getData("/vin" ?result \'tran)\n'
        #          '  "vout_tran": !!str getData("/vout" ?result \'tran)\n'
        #          '  "vout_ac": !!str getData("/vout" ?result \'ac)\n',
        #  'type': 'str'}

        # where to find the information required?
        # -----------------------------------------
        # corners
        #   - perhaps by looking up $PDK_ROOT/sky130A/libs.tech/ngspice/corners/*.spice
        #
        # parameters / outputs ...
        #   - those should come from the TestbenchParams
        #

        default_env = ''  # TODO
        corners = ['tt']  # TODO
        parameters = {}  # TODO
        outputs = {}  # TODO

        return default_env, corners, parameters, outputs

    def get_testbench_info(self, tb_lib: str, tb_cell: str) -> Tuple[
                                                                   List[str],
                                                                   List[str],
                                                                   Dict[str, str],
                                                                   Dict[str, str]
                                                               ]:
        """Returns information about an existing testbench.

        Parameters
        ----------
        tb_lib : str
            testbench library.
        tb_cell : str
            testbench cell.

        Returns
        -------
        cur_envs : list[str]
            the current simulation environments.
        envs : list[str]
            a list of available simulation environments.
        parameters : dict[str, str]
            a list of testbench parameter values, represented as string.
        outputs : dict[str, str]
            a list of testbench output expressions.
        """
        # cmd = 'get_testbench_info("{tb_lib}" "{tb_cell}" {result_file})'
        # cmd = cmd.format(tb_lib=tb_lib,
        #                  tb_cell=tb_cell,
        #                  result_file='{result_file}')
        # output = yaml.load(self._eval_skill(cmd, out_file='result_file'), Loader=yaml.Loader)
        # return output['enabled_corners'], output['corners'], output['parameters'], output['outputs']
        raise NotImplementedError('TODO: no simulation support yet')

    def update_testbench(self,
                         lib: str,
                         cell: str,
                         parameters: Dict[str, str],
                         sim_envs: List[str],
                         config_rules: List[List[str]],
                         env_parameters: List[List[Tuple[str, str]]]):
        """Update the given testbench configuration.

        Parameters
        ----------
        lib : str
            testbench library.
        cell : str
            testbench cell.
        parameters : Dict[str, str]
            testbench parameters.
        sim_envs : List[str]
            list of enabled simulation environments.
        config_rules : List[List[str]]
            config view mapping rules, list of (lib, cell, view) rules.
        env_parameters : List[List[Tuple[str, str]]]
            list of param/value list for each simulation environment.
        """

        sch = self.xschem_library_manager.update_testbench(lib=lib,
                                                           cell=cell,
                                                           parameters=parameters,
                                                           sim_envs=sim_envs,
                                                           config_rules=config_rules,
                                                           env_parameters=env_parameters)

        # MJK: IMO the BAG2 sim API should be refactored,
        # as it's a bit overfitting on ADEXL views
        #
        # But for now:
        #   - OpensourceInterface(DbAcess).update_testbench() writes the netlist file
        #   - NGSpiceInterface(SimAccess).setup_sim_process expects it to exist
        from opensource.bag.interface.ngspice import NGSpiceSimulationRun
        sim_run = NGSpiceSimulationRun.for_cell(lib=lib, cell=cell)
        os.makedirs(sim_run.run_dir, exist_ok=True)
        sim_run.save_sweep_info(parameters=parameters, sim_env=sim_envs[0])
        sch.spice_netlist(path=sim_run.netlist_path, mode=NetlistMode.SIMULATION)

    def release_write_locks(self,
                            lib_name: str,
                            cell_view_list: List[Tuple[str, str]]):
        """Release write locks from all the given cells.

        Parameters
        ----------
        lib_name : string
            the library name.
        cell_view_list : List[(string, string)]
            list of cell/view name tuples.
        """
        return None

    def create_schematic_from_netlist(self,
                                      netlist: str,
                                      lib_name: str,
                                      cell_name: str,
                                      sch_view: Optional[str] = None,
                                      **kwargs: Any):
        """Create a schematic from a netlist.

        This is mainly used to create extracted schematic from an extracted netlist.

        Parameters
        ----------
        netlist : str
            the netlist file name.
        lib_name : str
            library name.
        cell_name : str
            cell_name
        sch_view : Optional[str]
            schematic view name.  The default value is implemendation dependent.
        **kwargs : Any
            additional implementation-dependent arguments.
        """
        # calview_config = self.db_config.get('calibreview', None)
        # use_calibreview = self.db_config.get('use_calibreview', True)
        # if calview_config is not None and use_calibreview:
        #     # create calibre view from extraction netlist
        #     cell_map = calview_config['cell_map']
        #     sch_view = sch_view or calview_config['view_name']
        #
        #     # create calibre view config file
        #     tmp_params = dict(
        #         netlist_file=netlist,
        #         lib_name=lib_name,
        #         cell_name=cell_name,
        #         calibre_cellmap=cell_map,
        #         view_name=sch_view,
        #     )
        #     content = self.render_file_template('calibreview_setup.txt', tmp_params)
        #     with open_temp(prefix='calview', dir=self.tmp_dir, delete=False) as f:
        #         fname = f.name
        #         f.write(content)
        #
        #     # delete old calibre view
        #     cmd = f'delete_cellview( "{lib_name}" "{cell_name}" "{sch_view}" )'
        #     self._eval_skill(cmd)
        #     # make extracted schematic
        #     calibre_root_version = os.path.basename(os.environ['MGC_HOME']).split('.')[0]
        #     calibre_year = int(calibre_root_version[-4:])
        #     if calibre_year > 2011:
        #         cmd = f'mgc_rve_load_setup_file( "{fname}" )'
        #     else:
        #         cmd0 = f'mgc_eview_globals->outputLibrary = "{lib_name}"'
        #         self._eval_skill(cmd0)
        #         cmd0 = f'mgc_eview_globals->schematicLibrary = "{lib_name}"'
        #         self._eval_skill(cmd0)
        #         cmd0 = f'mgc_eview_globals->cellMapFile = "{cell_map}"'
        #         self._eval_skill(cmd0)
        #         cmd0 = 'mgc_eview_globals->createUnmatchedTerminals = t'
        #         self._eval_skill(cmd0)
        #         # cmd0 = 'mgc_eview_globals->preserveDeviceCase = t'
        #         # self._eval_skill(cmd0)
        #         cmd0 = 'mgc_eview_globals->devicePlacementArrayed = t'
        #         self._eval_skill(cmd0)
        #         cmd0 = 'mgc_eview_globals->showCalviewDlg = nil'
        #         self._eval_skill(cmd0)
        #         cmd = f'mgc_rve_create_cellview("{netlist}")'
        #     self._eval_skill(cmd)
        # else:
        #     # get netlists to copy
        #     netlist_dir = os.path.dirname(netlist)
        #     netlist_files = self.checker.get_rcx_netlists(lib_name, cell_name)
        #     if not netlist_files:
        #         # some error checking.  Shouldn't be needed but just in case
        #         raise ValueError('RCX did not generate any netlists')
        #
        #     # copy netlists to a "netlist" subfolder in the CAD database
        #     cell_dir = self.get_cell_directory(lib_name, cell_name)
        #     targ_dir = os.path.join(cell_dir, 'netlist')
        #     os.makedirs(targ_dir, exist_ok=True)
        #     for fname in netlist_files:
        #         shutil.copy(os.path.join(netlist_dir, fname), targ_dir)
        #
        #     # create symbolic link as aliases
        #     symlink = os.path.join(targ_dir, 'netlist')
        #     try:
        #         os.remove(symlink)
        #     except FileNotFoundError:
        #         pass
        #     os.symlink(netlist_files[0], symlink)
        raise NotImplementedError('create_schematic_from_netlist not implemented yet')

    def get_cell_directory(self, lib_name: str, cell_name: str) -> str:
        """Returns the directory name of the given cell.

        Parameters
        ----------
        lib_name : str
            library name.
        cell_name : str
            cell name.

        Returns
        -------
        cell_dir : str
            path to the cell directory.
        """
        # # use yaml.load to remove outermost quotation marks
        # lib_dir = yaml.load(self._eval_skill(f'get_lib_directory( "{lib_name}" )'), Loader=yaml.Loader)
        # if not lib_dir:
        #     raise ValueError('Library %s not found.' % lib_name)
        # return os.path.join(lib_dir, cell_name)
        raise NotImplementedError('get_cell_directory not implemented yet')

    def create_verilog_view(self,
                            verilog_file: str,
                            lib_name: str,
                            cell_name: str,
                            **kwargs: Any):
        """Create a verilog view for mix-signal simulation.

        Parameters
        ----------
        verilog_file : str
            the verilog file name.
        lib_name : str
            library name.
        cell_name : str
            cell name.
        **kwargs : Any
            additional implementation-dependent arguments.
        """
        # # delete old verilog view
        # cmd = 'delete_cellview( "%s" "%s" "verilog" )' % (lib_name, cell_name)
        # self._eval_skill(cmd)
        # cmd = 'schInstallHDL("%s" "%s" "verilog" "%s" t)' % (lib_name, cell_name, verilog_file)
        # self._eval_skill(cmd)
        raise NotImplementedError('create_verilog_view not implemented yet')

    def instantiate_layout_pcell(self,
                                 lib_name: str,
                                 cell_name: str,
                                 view_name: str,
                                 inst_lib: str,
                                 inst_cell: str,
                                 params: Dict[str, Any],
                                 pin_mapping: Dict[str, str]):
        """Create a layout cell with a single pcell instance.

        Parameters
        ----------
        lib_name : str
            layout library name.
        cell_name : str
            layout cell name.
        view_name : str
            layout view name, default is "layout".
        inst_lib : str
            pcell library name.
        inst_cell : str
            pcell cell name.
        params : dict[str, any]
            the parameter dictionary.
        pin_mapping: dict[str, str]
            the pin mapping dictionary.
        """
        raise NotImplementedError("instantiate_layout_pcell not implemented yet")

    @staticmethod
    def sorted_layout_list(lib_name: str,
                           layout_list: list) -> list:
        """
        Ensures a deterministic order for the submitted layout_list,
        so that dumping will give a diffable YAML file

        Parameters
        ----------
        lib_name: str
            name of the library

        layout_list: list
            layout list, as passed to instantiate_layout

        Returns
        -------
        Sorted layout list in alphabetic order, but respecting the hierarchical cell dependency
        """
        # PHASE 1) collect hierarchical dependencies
        CellName = str
        CellLayout = list
        layout_by_name: Dict[CellName, CellLayout] = {}
        dependencies: Dict[CellName, Set[CellName]] = {}
        for cell_layout in layout_list:
            cell_name = cell_layout[0]
            sub_instances: List[bag.layout.core.InstanceInfo] = cell_layout[1]
            layout_by_name[cell_name] = cell_layout
            dependency_set: Set[str] = set()
            dependencies[cell_name] = dependency_set
            for instance in sub_instances:
                # NOTE: we are only concerned with deps within our new library (other libs expected to exist)
                if instance.lib != lib_name:
                    continue
                dependency_set.add(instance.cell)

        sorted_list = []

        # PHASE 2) isolate graph roots and emit them in alphabetical order
        while len(dependencies) > 0:
            emittable_cells: List[CellName] = []
            for (cell_name, cell_deps) in dependencies.items():
                if len(cell_deps) == 0:
                    emittable_cells.append(cell_name)
            for cell_name in emittable_cells:
                dependencies.pop(cell_name)
            for cell_deps in dependencies.values():
                cell_deps.difference_update(emittable_cells)
            emittable_cells.sort()
            sorted_list.extend([layout_by_name[cell_name] for cell_name in emittable_cells])

        return sorted_list

    @staticmethod
    def dump_layout_list(layout_list: list, output_yaml_path: str):
        import yaml

        with open(output_yaml_path, 'w') as f:
            yaml.dump(layout_list, f)

    def debug_layout_list_order_driven_issues(self,
                                              lib_path: str,
                                              lib_name: str,
                                              layout_list: list,
                                              layout_driver: str):
        """
        Helper method to debug layout issues.

        - Dump the layout_list as YAML (both in original and sorted/diffable versions)
        - Generate and save the GDS twice
            - once for original layout_list version (was already generated in instantiate_layout())
            - once for sorted layout_list version
        """
        import datetime
        timestamp = datetime.datetime.now().strftime('%Y_%m_%d_-_%H_%M_%S_%f')
        layout_dump_dir = os.path.join(os.environ['BAG_GENERATOR_ROOT'], 'layout_dumps', timestamp)

        os.makedirs(layout_dump_dir, exist_ok=True)

        print(f"\nSaved YAML dumps of layout_lists and generated layouts (both for sorted/unsorted layout_lists) to:\n"
              f"{layout_dump_dir}\n")

        self.dump_layout_list(layout_list,
                              output_yaml_path=os.path.join(layout_dump_dir, f'layout_list_orig.yaml'))

        sorted_layout_list = self.sorted_layout_list(lib_name, layout_list)
        self.dump_layout_list(sorted_layout_list,
                              output_yaml_path=os.path.join(layout_dump_dir, f'layout_list_sorted.yaml'))

        input_gds_path = os.path.join(os.environ['BAG_GENERATOR_ROOT'], f'{lib_name}.gds')
        output_gds_path = os.path.join(layout_dump_dir, f'{lib_name}_orig.gds')
        shutil.copy(input_gds_path, output_gds_path)

        layout_generator = make_layout_generator(layout_driver, self.config)
        layout_generator.generate(lib_path, lib_name, sorted_layout_list)
        output_gds_path = os.path.join(layout_dump_dir, f'{lib_name}_sorted.gds')
        shutil.copy(input_gds_path, output_gds_path)

    def instantiate_layout(self,
                           lib_name: str,
                           view_name: str,
                           via_tech: str,
                           layout_list: List):
        """Create a batch of layouts.

        Parameters
        ----------
        lib_name : str
            layout library name.
        view_name : str
            layout view name.
        via_tech : str
            via technology library name.
        layout_list : list[any]
            a list of layouts to create
        """

        # TODO: no argument for specific lib path?
        lib_path = self.default_lib_path

        # create the open source layout generator and execute it
        layout_driver = None
        if "layout_driver" in self.config:
            layout_driver = self.config["layout_driver"]

        layout_generator = make_layout_generator(layout_driver, self.config)
        layout_generator.generate(lib_path, lib_name, layout_list)
        
        # NOTE: uncomment the next line, if you want to debug the final layout
        # self.debug_layout_list_order_driven_issues(lib_path, lib_name, layout_list, layout_driver)
