# -*- coding: utf-8 -*-

"""This module implements bag's interaction with an ngspice simulator.
"""

from __future__ import annotations
from dataclasses import dataclass
from functools import cached_property
from typing import *
import os

import bag.io
from bag.interface.simulator import SimProcessManager, ProcInfo


@dataclass
class NGSpiceSimulationRun:
    lib: str
    cell: str

    @cached_property
    def run_dir(self) -> str:
        return os.path.join(os.environ['BAG_GENERATOR_ROOT'], "simulations", self.lib, self.cell)

    @cached_property
    def netlist_path(self) -> str:
        return os.path.join(self.run_dir, f"{self.lib}_{self.cell}.spice")

    @cached_property
    def output_path(self) -> str:
        return os.path.join(self.run_dir, 'ngspice_output.raw')

    @cached_property
    def log_path(self) -> str:
        return os.path.join(self.run_dir, 'ngspice_run.log')

    @cached_property
    def sweep_info_path(self) -> str:
        return os.path.join(self.run_dir, 'sweep.info')

    @classmethod
    def for_cell(cls, lib: str, cell: str) -> NGSpiceSimulationRun:
        run = NGSpiceSimulationRun(lib=lib, cell=cell)
        return run

    def save_sweep_info(self,
                        parameters: Dict[str, str],
                        sim_env: str):
        line1 = "corner "
        line2 = f"{sim_env} "
        for key, value in parameters.items():
            line1 += f"{key} "
            line2 += f"{value} "
        line1 += '\n'
        line2 += '\n'
        with open(self.sweep_info_path, "w") as f:
            f.writelines([line1, line2])


class NGSpiceInterface(SimProcessManager):
    """This class handles interaction with ngspice simulators.

    Parameters
    ----------
    tmp_dir : str
        temporary file directory for SimAccess.
    sim_config : Dict[str, Any]
        the simulation configuration dictionary.
    """

    def __init__(self,
                 tmp_dir: str,
                 sim_config: Dict[str, Any]):
        super().__init__(tmp_dir=tmp_dir,
                         sim_config=sim_config)

    def format_parameter_value(self,
                               param_config: Dict[str, Any],
                               precision: int) -> str:
        """Format the given parameter value as a string.

        To support both single value parameter and parameter sweeps, each parameter value is represented
        as a string instead of simple floats.  This method will cast a parameter configuration (which can
        either be a single value or a sweep) to a simulator-specific string.

        Parameters
        ----------
        param_config: Dict[str, Any]
            a dictionary that describes this parameter value.

            4 formats are supported.  This is best explained by example.

            single value:
            dict(type='single', value=1.0)

            sweep a given list of values:
            dict(type='list', values=[1.0, 2.0, 3.0])

            linear sweep with inclusive start, inclusive stop, and step size:
            dict(type='linstep', start=1.0, stop=3.0, step=1.0)

            logarithmic sweep with given number of points per decade:
            dict(type='decade', start=1.0, stop=10.0, num=10)

        precision : int
            the parameter value precision.

        Returns
        -------
        param_str : str
            a string representation of param_config
        """
        fmt = '%.{}e'.format(precision)
        swp_type = param_config['type']
        match swp_type:
            case 'single':
                return fmt % param_config['value']

            case 'list':
                return ' '.join((fmt % val for val in param_config['values']))

            case 'linstep':
                raise NotImplementedError('TODO: not yet implemented')

                syntax = '{From/To}Linear:%s:%s:%s{From/To}' % (fmt, fmt, fmt)
                return syntax % (param_config['start'], param_config['step'], param_config['stop'])

            case 'decade':
                raise NotImplementedError('TODO: not yet implemented')

                syntax = '{From/To}Decade:%s:%s:%s{From/To}' % (fmt, '%d', fmt)
                return syntax % (param_config['start'], param_config['num'], param_config['stop'])

            case _:
                raise Exception('Unsupported param_config: %s' % param_config)

    def _get_ngspice_info(self,
                          simulation_run: NGSpiceSimulationRun):
        """Private helper function that launches ngspice process."""
        # get the simulation command.
        sim_kwargs = self.sim_config['kwargs']
        cfg_cmd = sim_kwargs['command']
        env = sim_kwargs.get('env', None)
        cwd = sim_kwargs.get('cwd', None)
        sim_cmd = [
            cfg_cmd,
            '--batch',
            '--raw', simulation_run.output_path,
            # simulation_run.log_path,
            simulation_run.netlist_path
        ]

        if cwd is None:
            cwd = simulation_run.run_dir

        # create empty log file to make sure it exists.
        return sim_cmd, simulation_run.log_path, env, cwd, simulation_run.run_dir

    def setup_sim_process(self,
                          lib: str,
                          cell: str,
                          outputs: Dict[str, str],
                          precision: int,
                          sim_tag: Optional[str]) -> ProcInfo:
        # MJK: IMO the BAG2 sim API should be refactored,
        # as it's a bit overfitting on ADEXL views
        #
        # But for now:
        #   - OpensourceInterface(DbAcess).update_testbench() writes the netlist file
        #   - NGSpiceInterface(SimAccess).setup_sim_process expects it to exist

        simulation_run = NGSpiceSimulationRun.for_cell(lib=lib, cell=cell)
        return self._get_ngspice_info(simulation_run)

    def setup_load_process(self,
                           lib: str,
                           cell: str,
                           hist_name: str,
                           outputs: Dict[str, str],
                           precision: int) -> ProcInfo:
        # init_file = self.sim_config['init_file']
        # view = self.sim_config['view']
        #
        # # create temporary save directory and log/script names
        # save_dir = bag.io.make_temp_dir(prefix='%s_data' % hist_name, parent_dir=self.tmp_dir)
        # log_fname = os.path.join(save_dir, 'ocn_output.log')
        # script_fname = os.path.join(save_dir, 'run.ocn')
        #
        # # setup ngspice load script
        # script = self.render_file_template('load_results.ocn',
        #                                    dict(
        #                                        lib=lib,
        #                                        cell=cell,
        #                                        view=view,
        #                                        init_file=init_file,
        #                                        save_dir=save_dir,
        #                                        precision=precision,
        #                                        hist_name=hist_name,
        #                                        outputs=outputs,
        #                                    ))
        # bag.io.write_file(script_fname, script)
        #
        # # launch ngspice
        # return self._get_ngspice_info(save_dir, script_path, output_path, log_path)
        raise NotImplementedError('TODO: not yet implemented')
