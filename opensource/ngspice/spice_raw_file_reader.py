#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import annotations  # allow class type hints within same class

from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from functools import cached_property
import io
import os
from typing import *
import unittest

import numpy as np

from sal.log import warn, error

#
# ngSPICE RAW file format information
# ------------------------------------
#   - https://github.com/imr/ngspice/blob/master/src/frontend/rawfile.c
#


@dataclass
class SPICEVariable:
    name: str
    type: str


class NumberDataType(Enum):
    REAL = 1
    COMPLEX = 2
    DEFAULT = REAL


class Padding(Enum):
    PADDED = 1
    UNPADDED = 2
    DEFAULT = PADDED


class DataFileFormat(Enum):
    ASCII = 1
    BINARY = 2


@dataclass
class SPICEVector:
    variable: SPICEVariable
    data: np.array


@dataclass
class SPICEPlot:
    title: str
    date: str
    plot_name: str
    plot_type: str

    number_data_type: NumberDataType
    padding: Padding

    number_of_points: int
    number_of_variables: int
    variables: List[SPICEVariable]

    scale_vector: SPICEVector
    data_vectors: List[SPICEVector]   # NOTE: one vector per point


@dataclass
class SPICERawContent:
    plots: List[SPICEPlot]

    @classmethod
    def read_file(cls, path: str) -> SPICERawContent:
        content: SPICERawContent
        plots: List[SPICEPlot] = []

        read_ahead_line: Optional[str] = None
        with open(path, 'rb') as f:
            while True:
                plot, read_ahead_line = cls.read_plot(file=f, read_ahead_line=read_ahead_line)
                if plot:
                    plots.append(plot)
                else:
                    break

        content = SPICERawContent(plots=plots)
        return content

    @classmethod
    def read_plot(cls, file: BinaryIO, read_ahead_line: Optional[str]) -> Tuple[Optional[SPICEPlot], Optional[str]]:
        """
        Read the next plot from a SPICE raw file.

        Parameters
        ----------
        file
            open SPICE raw file to parse
        read_ahead_line
            read ahead line from earlier parsing steps
        Returns
        -------
        Tuple, consisting of the parsed SPICEPlot, and the next read ahead line
        """
        # NOTE: start with an empty plot object, fields will get populated during parsing
        plot = SPICEPlot(title='',
                         date='',
                         plot_name='',
                         plot_type='',
                         number_data_type=NumberDataType.DEFAULT,
                         padding=Padding.DEFAULT,
                         number_of_points=0,
                         number_of_variables=0,
                         variables=[],
                         scale_vector=SPICEVector(variable=SPICEVariable(name='', type=''), data=np.zeros(0)),
                         data_vectors=[])
        while True:
            line = read_ahead_line if read_ahead_line else file.readline()
            if len(line) == 0:
                plot = None
                line = None
                break

            first_colon_idx = line.find(b':')
            key = line[0:first_colon_idx].strip().lower()
            val = line[first_colon_idx+1:].strip()

            match key:
                case b'title':
                    plot.title = val.decode('utf-8')
                case b'date':
                    plot.date = val.decode('utf-8')
                case b'plotname':
                    plot.plot_name = val.decode('utf-8')
                case b'flags':
                    flag_tokens = [token.strip().lower() for token in val.split()]
                    for flag in flag_tokens:
                        match flag:
                            case b'complex':
                                plot.number_data_type = NumberDataType.COMPLEX
                            case b'real':
                                plot.number_data_type = NumberDataType.REAL
                            case b'padded':
                                plot.padding = Padding.PADDED
                            case _:
                                warn(f"WARNING: unknown flag '{flag}'")
                case b'no. variables':
                    plot.number_of_variables = int(val.decode('utf-8'))
                case b'no. points':
                    plot.number_of_points = int(val.decode('utf-8'))
                case b'dimensions' | b'option' | b'command':
                    raise NotImplementedError(f"TODO: implement {key}")
                case b'offset':  # not supported by ngspice
                    warn(f"WARNING: {key} is not supported")
                case b'variables':
                    plot.variables = cls.read_variables(file=file,
                                                        expected_number_of_variables=plot.number_of_variables)
                case b'values':
                    data = cls.read_ascii_values(file=file,
                                                 number_of_variables=plot.number_of_variables,
                                                 number_of_points=plot.number_of_points,
                                                 number_data_type=plot.number_data_type)
                    plot.scale_vector, plot.data_vectors = cls.build_vectors(variables=plot.variables,
                                                                             data=data,
                                                                             number_data_type=plot.number_data_type)
                    line = None
                    break
                case b'binary':
                    data = cls.read_binary_data(file=file,
                                                number_of_variables=plot.number_of_variables,
                                                number_of_points=plot.number_of_points,
                                                number_data_type=plot.number_data_type)
                    plot.scale_vector, plot.data_vectors = cls.build_vectors(variables=plot.variables,
                                                                             data=data,
                                                                             number_data_type=plot.number_data_type)
                    line = None
                    break
                case _:
                    warn(f"WARNING: unexpected {key} is not supported")

        return plot, line

    @classmethod
    def read_variables(cls,
                       file: BinaryIO,
                       expected_number_of_variables: int) -> List[SPICEVariable]:
        """
        Reads the Variables table

        Examples
        --------

        No. Variables: 27
        No. Points: 101
        Variables:
                0       frequency       frequency grid=3
                1       v(m.xxdut.xxdummy0.xm1.msky130_fd_pr__nfet_01v8#body)   voltage
                ...
                21      v(vindc)        voltage
                22      i(vindc)        current
                23      v(vout) voltage
                24      v(vss)  voltage
                25      i(vsup) current
                26      i(vvbias)       current
        Binary:
        ...

        Parameters
        ----------
        file
            open SPICE raw file to read from
        expected_number_of_variables
            already parsed number of variables

        Returns
        -------
        list of SPICEVariable instances
        """

        variables: List[SPICEVariable] = []

        while len(variables) < expected_number_of_variables:
            line = file.readline()
            cells = [line.strip().decode('utf-8') for line in line.split()]
            if len(cells) < 3:
                error("ERROR: expected analysis variable line to have at least 3 table cells, but obtained: {line}")
                continue
            variable = SPICEVariable(
                name=cells[1],
                type=cells[2]
            )
            variables.append(variable)

        return variables

    @classmethod
    def read_binary_data(cls,
                         file: BinaryIO,
                         number_of_variables: int,
                         number_of_points: int,
                         number_data_type: NumberDataType) -> np.array:
        num_floats: int
        match number_data_type:
            case NumberDataType.REAL:
                num_floats = 1
            case NumberDataType.COMPLEX:
                num_floats = 2
            case _:
                raise NotImplementedError(f"unexpected NumberDataType value {number_data_type}")

        buffer_size = number_of_variables * number_of_points * num_floats * 8  # NOTE: 64-bit floats

        buffer = file.read(buffer_size)
        arr = np.frombuffer(buffer, dtype='float64')
        data = arr.reshape(number_of_points, number_of_variables * num_floats)
        return data

    @classmethod
    def read_ascii_values(cls,
                          file: BinaryIO,
                          number_of_variables: int,
                          number_of_points: int,
                          number_data_type: NumberDataType) -> np.array:
        raise NotImplementedError("TODO: ascii raw files not supported yet!")

    @classmethod
    def build_vectors(cls,
                      variables: List[SPICEVariable],
                      data: np.array,
                      number_data_type: NumberDataType) -> Tuple[SPICEVector, List[SPICEVector]]:
        data_vectors: List[SPICEVector] = []

        scale_vector = SPICEVector(variable=variables[0],
                                   data=data[:, 0])  # NOTE: for scale, we only use the real part!

        match number_data_type:
            case NumberDataType.REAL:
                for i in range(1, len(variables)):
                    data_vector = SPICEVector(variable=variables[i], data=data[:, i])
                    data_vectors.append(data_vector)

            case NumberDataType.COMPLEX:
                for i in range(1, len(variables)):
                    complex_array = np.array(data[:, 2*i] + 1j * data[:, 2*i + 1])
                    data_vector = SPICEVector(variable=variables[i], data=complex_array)
                    data_vectors.append(data_vector)

            case _:
                raise NotImplementedError(f"unexpected NumberDataType value {number_data_type}")

        return scale_vector, data_vectors


# --------------------------------------- tests ----------------------------------------
class TestSPICERawContent(unittest.TestCase):
    @cached_property
    def testdata_path_combined_tran_ac_raw(self) -> str:
        THIS_DIR = os.path.dirname(os.path.abspath(__file__))
        test_data_path = os.path.join(THIS_DIR, 'testdata', 'combined_tran_ac.raw')
        return test_data_path

    def test_read1(self):
        content = SPICERawContent.read_file(self.testdata_path_combined_tran_ac_raw)
        self.assertEqual(2, len(content.plots))
        if len(content.plots) == 2:
            plot0: SPICEPlot = content.plots[0]
            plot1: SPICEPlot = content.plots[1]
            self.assertEqual("Transient Analysis", plot0.plot_name)
            self.assertEqual(27, plot0.number_of_variables)
            self.assertEqual(100020, plot0.number_of_points)
            self.assertEqual(NumberDataType.REAL, plot0.number_data_type)
            self.assertEqual(Padding.PADDED, plot0.padding)
            self.assertEqual(27, len(plot0.variables))
            self.assertEqual("time", plot0.scale_vector.variable.name)
            self.assertEqual(26, len(plot0.data_vectors))

            self.assertEqual("AC Analysis", plot1.plot_name)
            self.assertEqual(27, plot1.number_of_variables)
            self.assertEqual(101, plot1.number_of_points)
            self.assertEqual(NumberDataType.COMPLEX, plot1.number_data_type)
            self.assertEqual(Padding.PADDED, plot0.padding)
            self.assertEqual(27, len(plot1.variables))
            self.assertEqual("frequency", plot1.scale_vector.variable.name)
            self.assertEqual(26, len(plot1.data_vectors))
        print(content)


if __name__ == '__main__':
    unittest.main()