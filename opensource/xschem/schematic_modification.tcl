proc get_instance_labels_by_pin_name {inst} {
  set labels_by_pin_name {}

  set ninst [xschem get instances]
  foreach p [xschem instance_pins $inst] {
    set l [xschem instance_pin_coord $inst name $p ]
    set px [lindex $l 1]
    set py [lindex $l 2]
    for {set i 0} { $i < $ninst} { incr i} {
      set iname [xschem getprop instance $i name]
      if {$iname == $inst} {continue}
      set type [xschem getprop instance $i cell::type]
      if { ! ($type == {label} || $type == {ipin} || $type == {opin} || $type == {iopin}) } {
        continue
      }
      foreach pp [xschem instance_pins $i] {
        set ll [xschem instance_pin_coord $i name $pp ]
        set ppx [lindex $ll 1]
        set ppy [lindex $ll 2]
        if {$ppx == $px && $ppy == $py} {
           dict set labels_by_pin_name $p $iname
        }
      }
    }
  }

  return $labels_by_pin_name
}

proc select_instance_and_labels {inst} {
  set labels_by_pin_name [get_instance_labels_by_pin_name $inst]

  dict for {pin_name iname} $labels_by_pin_name {
    xschem select instance $iname
  }
  xschem select instance $inst
}

proc select_dangling_wires {} {
  xschem search regex 1 lab {#net[0-9]+$}
}

proc delete_instance_and_labels {inst} {
  xschem unselect_all
  xschem select instance $inst
  xschem connected_nets
  xschem delete
}

proc add_instance_pin_label {inst pin_name net_name} {
  set rotation [expr {$pin_name == "B" ? 2 : 0}]

  set coord [xschem instance_pin_coord $inst name $pin_name]
  set x [lindex $coord 1]
  set y [lindex $coord 2]

  set params [list name=l1 lab=$net_name]

  xschem instance [pin_label] $x $y $rotation 0 $params
}

proc update_instance_pin_label {inst pin_name net_name} {
  set labels_by_pin_name [get_instance_labels_by_pin_name $inst]
  puts $labels_by_pin_name

  if {[dict exists $labels_by_pin_name $pin_name]} {
     set label_inst [dict get $labels_by_pin_name $pin_name]
     xschem setprop instance $label_inst lab $net_name
  } else {
      add_instance_pin_label $inst $pin_name $net_name
  }
}

