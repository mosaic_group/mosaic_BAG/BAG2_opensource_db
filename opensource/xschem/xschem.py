from __future__ import annotations
from dataclasses import dataclass
from distutils import spawn
from enum import Enum
from functools import cached_property
import os
from pathlib import Path
import shutil
import re
import subprocess
import tempfile
from textwrap import dedent
from typing import *

import cProfile
import time

import yaml


class NetlistMode(str, Enum):
    SIMULATION = 'sim'
    LVS = 'lvs'


@dataclass
class XSchemInstance:
    lib_name: str
    cell_name: str

    @classmethod
    def from_xschem_instance(cls, inst_path: str) -> XSchemInstance:
        # Instance Info Examples:
        #     devices/iopin.sym
        #     BAG_prim/nmos4_lvt/nmos4_lvt.sym
        #     inverter2_gen/inverter2_templates_xschem/inverter2/inverter2.sym
        path_components = inst_path.split(os.path.sep)
        lib_name: str

        # builtin_libraries = {'devices', os.environ['TECHLIB'], 'BAG_prim'}
        if len(path_components) == 2:
            lib_name = path_components[0]
        else:
            lib_name = path_components[-3]
            if lib_name.endswith('_xschem'):
                lib_name = lib_name[:-len('_xschem')]
        cell_name = path_components[-1][0:-len(".sym")]
        return XSchemInstance(lib_name=lib_name, cell_name=cell_name)

    @cached_property
    def symbol_path(self):
        return self.build_symbol_path(lib_name=self.lib_name, cell_name=self.cell_name)

    @classmethod
    def from_parsed_netlist(cls,
                            netlist: Dict[str, Any],
                            instance_name: str) -> XSchemInstance:
        """
        instances:
            XN:
                lib_name: BAG_prim
                cell_name: nmos4_lvt
                instpins:
                    ...
        """
        d = netlist['instances'][instance_name]
        lib_name = d['lib_name']
        cell_name = d['cell_name']
        return XSchemInstance(lib_name=lib_name,
                              cell_name=cell_name)

    @classmethod
    def build_symbol_path(cls,
                          lib_name: str,
                          cell_name: str) -> str:
        # TODO: ATM, the path is only done correctly for BAG_prim
        path = f"{lib_name}/{cell_name}/{cell_name}.sym"
        return path


class XSchemSchematic:
    def __init__(self,
                 schematic_file: str,
                 xschemrc_file: str,
                 lib_name: str,
                 cell_name: str):
        self.schematic_file = schematic_file
        self.xschemrc_file = xschemrc_file
        self.lib_name = lib_name
        self.cell_name = cell_name

        self.modification_date = os.path.getmtime(schematic_file)

    @cached_property
    def content(self) -> str:
        with open(self.schematic_file) as file:
            return file.read()

    @cached_property
    def pinlist(self) -> List[str]:
        pins = re.findall(r"C {devices\/[io]*pin.sym}.*lab=(.*)}", self.content)
        return pins

    @cached_property
    def netlist_yaml(self) -> str:
        pins = self.pinlist

        # cell top level info
        template = ""
        template += f"lib_name: {self.lib_name}\n"
        template += f"cell_name: {self.cell_name}\n"
        template += f"""pins: [{', '.join([f'"{_}"' for _ in pins])}]\n"""

        tcl_script_path = os.path.join(os.path.dirname(__file__), 'print_netlist_instances_yaml.tcl')
        result = xschem_call(tcl_script=tcl_script_path,
                             output_processing=OutputProcessing.FULL,
                             schematic=self.schematic_file,
                             xschemrc_file=self.xschemrc_file)
        template += result
        return template

    @cached_property
    def netlist(self) -> Dict[str, Any]:
        netlist = yaml.safe_load(self.netlist_yaml)
        return netlist

    def spice_netlist(self,
                      path: str,
                      mode: NetlistMode):
        tcl_command = ""

        match mode:
            case NetlistMode.LVS:
                tcl_command += "xschem set format lvs_format"
            case NetlistMode.SIMULATION:
                pass  # we leave default 'format' attribute as the source for netlist spice formats

        path = os.path.abspath(path)
        tcl_command += f"xschem netlist \"{path}\""

        xschem_call(
            tcl_command=tcl_command,
            schematic=self.schematic_file,
            xschemrc_file=self.xschemrc_file
        )

    @cached_property
    def instances_names(self) -> List[str]:
        names = list(self.netlist['instances'].keys())
        return names

    def instance(self, name: str) -> XSchemInstance:
        return XSchemInstance.from_parsed_netlist(self.netlist, name)

    def get_terminals_of_inst(self, inst_name: str) -> List[str]:
        """
        Returns list of terminal pins of an instance present in schematic
        """
        inst_pins = list(self.netlist['instances'][inst_name]['instpins'].keys())
        return inst_pins
    
    def get_terminal_connections_of_inst(self, inst_name: str) -> Dict[str, str]:
        """
        Returns terminal of the instance & net connected to that terminal in the schematic template, as 
        key : value pairs 
        """
        terminals = self.get_terminals_of_inst(inst_name)
        term_connections = {
            terminal: self.netlist['instances'][inst_name]['instpins'][terminal]['net_name']
                      for terminal in terminals
        }
        return term_connections


class XSchemLibraryManager:
    """
    NOTE: initializer is private,
          use XSchemLibraryManager.cached_instance() to access instances
    """
    __private_initializer_key = object()

    def __init__(self,
                 private_initializer_key: object,
                 xschemrc_file: str):
        assert (private_initializer_key == self.__private_initializer_key), \
           "initializer is private, use XSchemLibraryManager.cached_instance() to access instances"
        self.xschemrc_file = xschemrc_file
        self.cached_schematics: Dict[str, XSchemSchematic] = {}
        self.cached_libraries: Optional[List[str]] = None

    @classmethod
    def cached_instance(cls,
                        xschemrc_file: str):
        return XSchemLibraryManager(private_initializer_key=cls.__private_initializer_key,
                                    xschemrc_file=xschemrc_file)

    def update_from_rc_template(self,
                                xschemrc_template_file: str):
        """
        If not yet existent, xschemrc will be created based on the template xschemrc.in.
        """
        project_dir_path = os.path.dirname(self.xschemrc_file)
        os.makedirs(project_dir_path, exist_ok=True)

        if not os.path.exists(self.xschemrc_file):
            shutil.copy(xschemrc_template_file, self.xschemrc_file)
            return project_dir_path

        template_libs = xschem_get_libraries(xschemrc_file=xschemrc_template_file)
        user_libs = xschem_get_libraries(xschemrc_file=self.xschemrc_file)
        additional_user_libs = list(set(user_libs).difference(template_libs))

        was_template_updated = os.path.getmtime(xschemrc_template_file) > os.path.getmtime(self.xschemrc_file)

        if was_template_updated:
            # NOTE: in case the template xschemrc was updated, we recreate the user's xschemrc
            #       while retaining the user's added libraries

            shutil.copy(xschemrc_template_file, self.xschemrc_file)
            for lib in additional_user_libs:
                lib_path = os.path.dirname(lib)
                lib_name = os.path.basename(lib)
                self.add_library(lib_name=lib_name, lib_path=lib_path)

        return project_dir_path

    def schematic(self,
                  lib_name: str,
                  cell_name: str) -> XSchemSchematic:
        schematic_file = self.schematic_filename(lib_name=lib_name, cell_name=cell_name)
        cached_sch: Optional[XSchemSchematic] = self.cached_schematics.get(schematic_file, None)

        if cached_sch:
            current_modification_date = os.path.getmtime(schematic_file)
            if cached_sch.modification_date <= current_modification_date:
                return cached_sch

        sch = XSchemSchematic(schematic_file=schematic_file,
                              xschemrc_file=self.xschemrc_file,
                              lib_name=lib_name,
                              cell_name=cell_name)
        self.cached_schematics[schematic_file] = sch
        return sch

    def resolve_lib_path(self,
                         lib_name: str) -> str:
        match = re.match(r'(?P<cell>.*)_templates(_xschem)?', lib_name)
        if match:
            cell_name = match.group('cell')
            suffix: str
            if cell_name.endswith('_tb'):  # this library is a testbench package
                suffix = ''
            else:  # this library is a generator package
                suffix = '_gen'
            lib_path = os.path.join(os.environ['BAG_GENERATOR_ROOT'],
                                    f'{cell_name}{suffix}',
                                    f'{cell_name}_templates_xschem')
            return lib_path

        # xschemrc = os.path.join(lib_path, "xschemrc")
        # libraries = get_libraries(xschemrc=xschemrc)
        libraries = self.libraries()
        for library in libraries:
            candidate = os.path.join(library, lib_name)
            if os.path.isdir(candidate):
                return candidate

        raise Exception(f"Library {lib_name} not found. Check this file: {self.xschemrc_file}")

    def schematic_filename(self,
                           lib_name: str,
                           cell_name: str,
                           ext: str = "sch") -> str:
        lib_path = self.resolve_lib_path(lib_name)
        sch_file = os.path.join(lib_path, cell_name, f'{cell_name}.{ext}')
        return sch_file

    def xschem_import_path(self,
                           lib_name: str,
                           cell_name: str) -> str:
        ext = "sym"
        if lib_name == "devices":
            import_path = os.path.join(lib_name, f'{cell_name}.{ext}')
            return import_path
        return self.schematic_filename(lib_name=lib_name, cell_name=cell_name, ext=ext)

    def libraries(self) -> List[str]:
        if not self.cached_libraries:
            self.cached_libraries = xschem_get_libraries(xschemrc_file=self.xschemrc_file)
        return self.cached_libraries

    def cells_of_library(self, lib_name: str) -> List[str]:
        cell_list: List[str] = []

        lib_path = self.resolve_lib_path(lib_name)

        for item in os.listdir(lib_path):
            subpath = os.path.join(lib_path, item)
            if os.path.isdir(subpath):
                cell_list.append(item)

        return cell_list

    def add_library(self,
                    lib_name: str,
                    lib_path: str,
                    replace_if_exists: bool = False):
        if replace_if_exists:
            path = f"{lib_path}/{lib_name}"
            if os.path.exists(path):
                shutil.rmtree(path)
            os.makedirs(path)

        already_added: bool
        with open(self.xschemrc_file, "r") as file:
            xschemrc = file.read()
            already_added = lib_name in xschemrc

        print("lib_path: ", lib_path)

        if not already_added:
            with open(self.xschemrc_file, "a") as file:
                if lib_path == "":
                    lib_path = "$env(BAG_GENERATOR_ROOT)"
                file.write(f"append XSCHEM_LIBRARY_PATH :{lib_path}/{lib_name}\n")
            self.cached_libraries = None  # invalidate library cache

    @staticmethod
    def _map_pin_name(cell_name: str,
                      pin_name: str) -> str:
        # NOTE: in Virtuoso voltage/current sources use "MINUS" and "PLUS" as terminals,
        #       while xschem vsource/isource use "p" and "m",
        #       so we map those
        match cell_name, pin_name:
            case("vsource", "MINUS") | ("isource", "MINUS"):
                pin_name = "m"
            case("vsource", "PLUS") | ("isource", "PLUS"):
                pin_name = "p"
        return pin_name

    @staticmethod
    def _map_param_name(cell_name: str,
                        param_name: str) -> str:
        # NOTE: in Virtuoso voltage/current sources use "vdc" / "idc" as values,
        #       while xschem vsource/isource use "value",
        #       so we map those
        match cell_name, param_name:
            case("vsource", "vdc") | ("isource", "idc"):
                param_name = "value"
        return param_name

    def _create_concrete_schematic(self,
                                   template_schematic: XSchemSchematic,
                                   generated_schematic: XSchemSchematic,
                                   changes: Dict[str, Any]):
        tcl_command = ""

        for tcl_script in ('schematic_modification.tcl', 'math.tcl'):
            tcl_script_path = os.path.join(os.path.dirname(__file__), tcl_script)
            with open(tcl_script_path, "r") as f:
                tcl_command += f.read()
                tcl_command += "\n"

        for old_pin, new_pin in changes["pin_map"]:
            if old_pin != new_pin:
                raise NotImplementedError("Pin renaming not yet supported!")
                # sch_cell.rename_pin(old_pin, new_pin)

        # loop through instance changes
        preexisting_instance_names = generated_schematic.instances_names
        used_instances = []
        new_j = 0
        # loop through device changes
        for inst_name, rinst_list in changes["inst_list"]:
            new_i = 1
            new_j += 1
            terminals_of_inst = template_schematic.get_terminals_of_inst(inst_name)
            terminal_connections_of_inst = template_schematic.get_terminal_connections_of_inst(inst_name)

            for i, insert_instance in enumerate(rinst_list):
                new_name = insert_instance["name"]
                new_cell_name = insert_instance["cell_name"]
                new_lib_name = insert_instance["lib_name"]

                # NOTE: in Virtuoso voltage/current sources use "MINUS" and "PLUS" as terminals,
                #       while xschem vsource/isource use "p" and "m"
                #       so we map those
                canonical_term_mapping: List[Tuple[str, str]] = []
                for pin_name, net_name in insert_instance["term_mapping"]:
                    pin_name = self._map_pin_name(cell_name=new_cell_name, pin_name=pin_name)
                    canonical_term_mapping.append((pin_name, net_name))

                canonical_params_mapping: List[Tuple[str, str]] = []
                for param_name, value in insert_instance["params"]:
                    param_name = self._map_param_name(cell_name=new_cell_name, param_name=param_name)
                    canonical_params_mapping.append((param_name, value))

                # find terminals missing in term_mapping (according to schematic template)
                connected_terms = [term[0] for term in canonical_term_mapping]
                unconnected_terminals_connection_info = [
                    (inst_term, terminal_connections_of_inst[inst_term])
                    for inst_term in terminals_of_inst if inst_term not in connected_terms
                ]

                if new_name in preexisting_instance_names:
                    old_instance = template_schematic.instance(name=new_name)
                    if old_instance.cell_name != new_cell_name:
                        # NOTE: this is no longer true,
                        #       for concrete schematics we also replace the dut_model with the actual cell

                        # if old_instance.lib_name != "BAG_prim":
                        #     raise ValueError(
                        #         f"Replacing symbols is only intended for switching transistor thresholds, so "
                        #         f"expected lib_name '{old_instance.lib_name}', obtained lib_name '{new_lib_name}'"
                        #     )
                        new_sym_path = f"{new_lib_name}/{new_cell_name}/{new_cell_name}.sym"
                        tcl_command += f"set inst_no [xschem instance_pos {inst_name}]; " \
                                       f"xschem replace_symbol $inst_no \"{new_sym_path}\"; " \
                                       f"xschem setprop instance $inst_no name \"{new_name}\"; " \
                                       'if {[xschem getprop instance $inst_no model] != ""} { ' \
                                       f"xschem setprop instance $inst_no model {new_cell_name}; " \
                                       "};\n"
                else:  # new instance
                    # New instances are created for example when:
                    #    - dummy instances are added based on an existing reference
                    #    - signal source added based on an existing reference (i.e. on VSUP in a testbench schematic)
                    #
                    # Location of the new instances:
                    #    to the right of the existing instances (as it is done in Virtuoso use case)
                    #

                    insert_symbol = self.xschem_import_path(new_lib_name, new_cell_name)

                    tcl_command += f"set ref_inst_bbox [xschem instance_bbox {inst_name}]; \n" \
                                   f"set ref_left [lindex $ref_inst_bbox 1]; \n" \
                                   f"set ref_top [lindex $ref_inst_bbox 2]; \n" \
                                   f"set ref_right [lindex $ref_inst_bbox 3]; \n" \
                                   f"set ref_bottom [lindex $ref_inst_bbox 4]; \n" \
                                   f"set ref_width [expr $ref_right - $ref_left]; \n" \
                                   f"set ref_height [expr $ref_bottom - $ref_top]; \n" \
                                   f"set sym_x_offset [lindex $ref_inst_bbox 6]; \n" \
                                   f"set sym_y_offset [lindex $ref_inst_bbox 7]; \n" \
                                   f"# NOTE: we want placement on the grid (multiples of 10) \n" \
                                   f"set dx [snap_to_grid [expr ($ref_width * 3)] 10]; \n" \
                                   f"set x [expr $ref_left - $sym_x_offset + $dx * {new_i}]; \n" \
                                   f"set y [expr $ref_top - $sym_y_offset]; \n" \
                                   f"set x [snap_to_grid $x 10]; \n" \
                                   f"set y [snap_to_grid $y 10]; \n"

                    # insert new instance
                    tcl_command += f"xschem instance {insert_symbol} $x $y 0 0; \n" \
                                   "set last_inst_nr [expr [xschem get instances] -1]; \n" \
                                   f"xschem setprop instance $last_inst_nr name {new_name}; \n"

                    new_i += 1

                # add net labels
                for pin_name, net_name in canonical_term_mapping:
                    tcl_command += f"update_instance_pin_label {new_name} {pin_name} {net_name}; \n"

                for pin_name, net_name in unconnected_terminals_connection_info:
                    tcl_command += f"update_instance_pin_label {new_name} {pin_name} {net_name}; \n"

                # update (new or old) instance parameters
                for param_name, value in canonical_params_mapping:
                    tcl_command += f"xschem setprop instance {new_name} {param_name} \"{value}\"; \n"

                used_instances.append(insert_instance["name"])

        # delete unused template instances
        unused_instances = [instance for instance in preexisting_instance_names if instance not in used_instances]
        for instance in unused_instances:
            tcl_command += f"delete_instance_and_labels {instance}; \n"

        tcl_command += "xschem unselect_all\n" \
                       "xschem select_dangling_nets \n" \
                       "xschem delete \n"

        tcl_command += "xschem save\n"

        xschem_call(
            tcl_command=tcl_command,
            schematic=generated_schematic.schematic_file,
            xschemrc_file=self.xschemrc_file
        )

    @staticmethod
    def _replace_concrete_xschem_instance_symbols(in_file,
                                                  out_file,
                                                  instance_by_name: Dict[str, XSchemInstance]):
        # NOTE: we need to patch the instance paths. if a hierarchical generator template schematic has
        #       originally: inverter2_gen/inverter2_templates_xschem/inverter2/inverter2.sym
        #       ->
        #       patched:    mux_2to1_generated/inverter2/inverter2.sym
        #
        # Example (amp_cs_gen + ac_tran_tb):
        # ------------------------------------------------
        # Original line in the ac_tran_tb "abstract" template schematic:
        #       C {ac_tran_tb/ac_tran_tb_templates_xschem/dut_model/dut_model.sym} 280 -820 0 0 {name=XDUT
        # ->
        # Patched line in the ac_tran_tb "concrete" template schematic:
        #       C {amp_cs_generated/amp_cs_gen__ac_tran_tb/amp_cs_gen__ac_tran_tb.sym} 280 -820 0 0 {name=XDUT

        lines = in_file.readlines()
        for idx, line in enumerate(lines):
            match = re.match(r'.*/(?P<cell>.*)_templates_xschem/.*{name=(?P<inst_name>[a-zA-Z_0-9]*)', line)
            if match:
                cell = match.group('cell')
                inst_name = match.group('inst_name')
                old_symbol_path = f'{cell}_gen/{cell}_templates_xschem/{cell}/{cell}.sym'
                new_symbol_path = instance_by_name[inst_name].symbol_path
                patched_line = line.replace(old_symbol_path, new_symbol_path)
                lines[idx] = patched_line
        out_file.writelines(lines)

    def create_implementation_library(self,
                                      lib_name: str,
                                      template_list: List,
                                      change_list: List,
                                      lib_path: str = ""):
        """Create implementation of a design in the CAD database.

        Parameters
        ----------
        lib_name : str
            implementation library name.
        template_list : list
            a list of schematic templates to copy to the new library.
        change_list :
            a list of changes to be performed on each copied templates.
        lib_path : str
            directory to create the library in.  If Empty, use default location.
        """

        # create library in case it doesn't exist
        # NOTE: for testbench support we don't want to replace existing cells
        self.add_library(lib_name=lib_name, lib_path=lib_path, replace_if_exists=False)

        generated_schematic_list: List[XSchemSchematic] = []
        template_schematic_list: List[XSchemSchematic] = []

        # copy template schematic
        for i in range(len(template_list)):
            template_lib_name, template_cell_name, impl_cell_name = template_list[i]
            template_schematic_filename = self.schematic_filename(lib_name=template_lib_name,
                                                                  cell_name=template_cell_name)
            generated_lib_path = os.path.join(lib_path, lib_name)
            generated_cell_path = os.path.join(generated_lib_path, impl_cell_name)
            os.makedirs(generated_cell_path, exist_ok=True)
            generated_schematic_filename = os.path.join(generated_cell_path, f"{impl_cell_name}.sch")
            
            # Collect instance names and their corresponding implementation cell info
            instance_by_name: Dict[str, XSchemInstance] = {}
            for inst_name, rinst_list in change_list[i]["inst_list"]:
                if len(rinst_list) == 0:  # replacement instance list can be empty,
                    continue              # for example if testbench signal source instances are deleted

                inst_lib_name = rinst_list[0]["lib_name"]
                if inst_lib_name not in ['devices', 'BAG_prim']:
                    inst_cell_name = rinst_list[0]["cell_name"]
                    instance_by_name[inst_name] = XSchemInstance(lib_name=inst_lib_name, cell_name=inst_cell_name)

            with open(template_schematic_filename, 'r') as in_file:
                with open(generated_schematic_filename, 'w') as out_file:
                    self._replace_concrete_xschem_instance_symbols(in_file=in_file,
                                                                   out_file=out_file,
                                                                   instance_by_name=instance_by_name)

            template_symbol_filename = template_schematic_filename.split(".sch")[0] + ".sym"
            if os.path.exists(template_symbol_filename):
                shutil.copyfile(
                    template_symbol_filename, f"{lib_path}/{lib_name}/{impl_cell_name}/{impl_cell_name}.sym"
                )

            sch = XSchemSchematic(schematic_file=generated_schematic_filename,
                                  xschemrc_file=self.xschemrc_file,
                                  lib_name=lib_name,
                                  cell_name=template_cell_name)
            generated_schematic_list.append(sch)
            
            template_sch = XSchemSchematic(schematic_file=template_schematic_filename,
                                           xschemrc_file=self.xschemrc_file,
                                           lib_name=lib_name,
                                           cell_name=template_cell_name)
            template_schematic_list.append(template_sch)

        # write schematic
        for template_schematic, generated_schematic, change_info in zip(template_schematic_list,
                                                                        generated_schematic_list,
                                                                        change_list):
            self._create_concrete_schematic(template_schematic=template_schematic,
                                            generated_schematic=generated_schematic,
                                            changes=change_info)
        return "t"

    @staticmethod
    def _render_spice_codeblock(parameters: Dict[str, str],
                                sim_env: str) -> str:
        spice_codeblock = f"* NOTE: auto-generated code block,\n" \
                          f"*       created by gen sim during\n" \
                          f"*       concrete testbench instantiation\n" \
                          f"\n"

        # TODO: make this process agnostic
        spice_codeblock += f"* technology-specific code\n" \
                           f"** see https://github.com/iic-jku/osic-multitool/tree/main#spice-model-file-reducer\n" \
                           f".lib sky130.lib.spice.tt.red {sim_env}\n" \
                           f".param mc_mm_switch=0\n" \
                           f"\n"

        # NOTE: we're also making the variables available in the .control sections
        #       as we do all the (multiple) analyses in those
        spice_codeblock += f"* parameter declarations\n"
        for key, value in parameters.items():
            spice_codeblock += f".param {key}={value}\n" \
                               f".csparam {key}={'{'}{key}{'}'}\n"

        return spice_codeblock.strip()

    def update_testbench(self,
                         lib: str,
                         cell: str,
                         parameters: Dict[str, str],
                         sim_envs: List[str],
                         config_rules: List[List[str]],
                         env_parameters: List[List[Tuple[str, str]]]) -> XSchemSchematic:
        """Update the given testbench configuration.

        Parameters
        ----------
        lib : str
            testbench library.
        cell : str
            testbench cell.
        parameters : Dict[str, str]
            testbench parameters.
        sim_envs : List[str]
            list of enabled simulation environments.
        config_rules : List[List[str]]
            config view mapping rules, list of (lib, cell, view) rules.
        env_parameters : List[List[Tuple[str, str]]]
            list of param/value list for each simulation environment.

        Returns
            the updated schematic
        """

        sch = self.schematic(lib_name=lib, cell_name=cell)

        sim_env = sim_envs[0].lower()  # e.g. 'tt'

        spice_codeblock = self._render_spice_codeblock(parameters=parameters, sim_env=sim_env)

        # # NOTE: for now we expect a code block instance called SPICE_PARAMS

        tcl_command = ""
        tcl_command += "set value \""
        tcl_command += spice_codeblock.replace('\"', '\\\"')
        tcl_command += "\"\n"

        tcl_command += "xschem setprop instance SPICE_PARAMS value $value\n" \
                       "xschem save\n"

        xschem_call(
            tcl_command=tcl_command,
            schematic=sch.schematic_file,
            xschemrc_file=self.xschemrc_file
        )

        return sch


class OutputProcessing(str, Enum):
    FULL = 'full'
    LAST_LINE = 'last_line'
    LAST_LINE_SPACE_SPLITTED = 'last_line_space_splitted'


def xschem_render_command_line(xschemrc_file: str,
                               tcl_script: Optional[str] = None,   # either a script ...
                               tcl_command: Optional[str] = None,  # ... or a command string
                               schematic: Optional[str] = None,
                               folder: Optional[str] = None,
                               topcell: Optional[str] = None) -> List[str]:
    xschem_path = os.getenv("XSCHEM_BIN", default=spawn.find_executable("xschem"))

    cmd: List[str] = [
        xschem_path,
        '-q',  # batch mode
        '-x',  # no X11
    ]

    if not xschemrc_file:
        xschemrc_file = os.path.join(cwd, 'xschem', 'xschemrc')

    cmd += ['--rcfile', xschemrc_file]

    # specify the output netlist
    if folder:
        folder = os.path.abspath(folder)
        cmd.extend(['-o', folder])

    if tcl_script:
        pass
    elif tcl_command:
        with tempfile.NamedTemporaryFile(mode="w",
                                         suffix=".tcl",
                                         delete=False,
                                         dir=os.environ["BAG_TEMP_DIR"]) as file:
            # print("Script contains TCL commands: ")
            # print(tcl_command)

            file.write(tcl_command)
            tcl_script = file.name
    else:
        raise ValueError("xschem_call: except either tcl_script or tcl_command argument")

    cmd += ['--script', tcl_script]

    # include subcircuit definition
    if topcell:
        cmd += ['--tcl', 'set', 'top_subckt', '1', '-n']

    # add the schematic
    if schematic:
        cmd += [schematic]

    return cmd


def xschem_call(tcl_script: Optional[str] = None,  # either a script ...
                tcl_command: Optional[str] = None,  # ... or a command string
                schematic: Optional[str] = None,
                folder: Optional[str] = None,
                topcell: Optional[str] = None,
                output_processing: OutputProcessing = OutputProcessing.LAST_LINE_SPACE_SPLITTED,
                cwd: str = ".",
                xschemrc_file: Optional[str] = None) -> str:
    cmd = xschem_render_command_line(tcl_script=tcl_script,
                                     tcl_command=tcl_command,
                                     schematic=schematic,
                                     folder=folder,
                                     topcell=topcell,
                                     xschemrc_file=xschemrc_file)

    print(f"\nExecuting command: {' '.join(cmd)}")

    status = subprocess.check_output(
        cmd,
        cwd=cwd,
    )

    output: str = status.decode("utf-8")

    match output_processing:
        case OutputProcessing.FULL:
            pass  # leave output unprocessed

        case OutputProcessing.LAST_LINE | OutputProcessing.LAST_LINE_SPACE_SPLITTED:
            response_last_line = output.splitlines()
            while "" in response_last_line:
                response_last_line.remove("")
            if len(response_last_line) > 0:
                response_last_line = response_last_line[-1]
            output = response_last_line

            if output_processing == OutputProcessing.LAST_LINE_SPACE_SPLITTED and isinstance(output, str):
                output = output.split(" ")

    return output


def xschem_get_libraries(xschemrc_file: str) -> List[str]:
    libraries = xschem_call(tcl_command="puts $pathlist", xschemrc_file=xschemrc_file)
    return libraries


def xschem_netlist_command(xschemrc_file: str,
                           schematic_path: str,
                           out_path: str,
                           mode: NetlistMode) -> List[str]:
    # build command
    tcl_command = "xschem set format lvs_format; "
    match mode:
        case NetlistMode.LVS:
            tcl_command += f"xschem netlist \"{out_path}\""
        case NetlistMode.SIMULATION:
            pass  # we stick with default parameter 'format' to define the spice format
        case _:
            raise NotImplementedError(f"unhandled enum case: {mode}")

    return xschem_render_command_line(tcl_command=tcl_command,
                                      xschemrc_file=xschemrc_file,
                                      schematic=schematic_path)
