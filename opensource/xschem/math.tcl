proc tcl::mathfunc::roundto {value decimalplaces} {
  expr {round(10**$decimalplaces * $value) / 10.0**$decimalplaces}
}

proc snap_to_grid {coord grid} {
  # NOTE: we round up coord to be a multiple of grid
  set c [expr int(ceil($coord))]
  set rem [expr {$c % $grid}]
  if {$rem >= 1} {
    set c [expr {$c + $grid - $rem}]
  }
  return $c
}
