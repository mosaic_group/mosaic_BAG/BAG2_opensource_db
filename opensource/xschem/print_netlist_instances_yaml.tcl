proc assert condition {
   set s "{$condition}"
   if {![uplevel 1 expr $s]} {
       return -code error "assertion failed: $condition"
   }
}

set DIRECTION_MAP [dict create "in" "input" "out" "output" "inout" "inputOutput"]

proc print_yaml_netlist {} {
    global DIRECTION_MAP

    # NOTE: the top level info part is generated in Python
    # top level cell info
    # puts "lib_name: $lib_name"
    # puts "cell_name: $cell_name"
    # puts "pins: \[$pins\]"

    # instances info
    set number_cells [xschem get instances]
    if {$number_cells == 0} {
        puts "instances: {}"
    } else {
        puts "instances:"
    }
    for {set i 0} {$i < $number_cells} {incr i} {
        set instance_name [xschem getprop instance $i name]
        if {[string length $instance_name] == 0} { continue }
        set cell_name [xschem getprop instance $i cell::name]

        # Instance Info Examples:
        #     devices/iopin.sym
        #     BAG_prim/nmos4_lvt/nmos4_lvt.sym
        #     inverter2_gen/inverter2_templates_xschem/inverter2/inverter2.sym
        #     ac_tran_tb/ac_tran_tb_templates_xschem/dut_model/dut_model.sym
        #
        set path_components [split $cell_name '/']

        # cell name: remove suffix .sym of last path component
        set last_path_component [lindex $path_components [expr [llength $path_components] - 1]]
        set inst_cell_name [string range $last_path_component 0 [expr [string length $last_path_component] - 5]]

        if {[llength $path_components] == 4} {
            set templates_dir [lindex $path_components 1]
            # remove _xschem suffix, i.e.,
            #     ac_tran_tb_templates_xschem -> ac_tran_tb_templates
            set length_without_suffix [expr [string length $templates_dir] - [string length "_xschem"] - 1]
            set inst_lib_name [string range $templates_dir 0 $length_without_suffix]
        } else {
            set inst_lib_name [lindex $path_components 0]
        }

        # NOTE: strings should be quoted, since YAML parses unquoted strings like 'On' or 'Off' as booleans
        #       see https://yaml.org/type/bool.html

        puts "  '$instance_name':"
        puts "    lib_name: '$inst_lib_name'"
        puts "    cell_name: '$inst_cell_name'"

        set inst_pin_names [xschem pinlist $i name]
        set spiceprefix [xschem getprop instance $i cell::spiceprefix]
        if {[llength $inst_pin_names] == 0 || [string range $spiceprefix 0 1] != "X"} {
            puts "    instpins: {}"
        } else {
            puts "    instpins: "

            set nodemap [xschem instance_nodemap $i]
            set net_map [lrange $nodemap 1 end]

            set inst_pin_dirs [xschem pinlist $i dir]
            assert {[llength $inst_pin_names] == [llength $inst_pin_dirs]}

            for {set j 0} {$j < [llength $inst_pin_names]} {incr j} {
                set name_entry [lindex $inst_pin_names $j]
                set dir_entry [lindex $inst_pin_dirs $j]
                set name_pin_number [lindex $name_entry 0]
                set dir_pin_number [lindex $dir_entry 0]
                assert {$name_pin_number == $dir_pin_number}
                set pin_name [lindex $name_entry 1]
                set pin_dir [lindex $dir_entry 1]

                puts "      '$pin_name': "

                set direction [dict get $DIRECTION_MAP $pin_dir]
                set net [dict get $net_map $pin_name]

                puts "        direction: '$direction'"
                puts "        net_name: '$net'"
                puts "        num_bits: 1"
            }
        }

        puts "  "
    }
}

print_yaml_netlist
